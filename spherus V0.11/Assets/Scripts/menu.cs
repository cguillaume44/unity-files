﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class menu : MonoBehaviour 
{
	public Canvas quitMenu;
	public Button startText;
	public string nom_start_scene;
	public Button exitText;
	public string nom_exit_scene;


	void Start ()

	{
		quitMenu = quitMenu.GetComponent<Canvas>();
		startText = startText.GetComponent<Button> ();
		exitText = exitText.GetComponent<Button> ();
		quitMenu.enabled = false;
	
	}

	public void ExitPress()

	{
		quitMenu.enabled = true;
		startText.enabled = false;
		exitText.enabled = false;
	
	}

	public void NoPress()
	
	{
		quitMenu.enabled = false;
		startText.enabled = true;
		exitText.enabled = true;
	
	}

	public void StartLevel ()

	{
		Application.LoadLevel (nom_start_scene);

	}
	public void exit_scene ()
		
	{
		Application.LoadLevel (nom_exit_scene);
		
	}


	public void ExitGame ()
		
	{
		Application.Quit();
		
	}





}
