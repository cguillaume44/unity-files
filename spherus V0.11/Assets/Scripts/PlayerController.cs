﻿using UnityEngine;
using System.Collections;
using CnControls;

public class PlayerController : MonoBehaviour {

	public DpadAxis Dpad;
	public float moveSpeed = 70;
	private Vector3 moveDirection;
	public int axisH;
	public int axisV;
	public bool canMove;
	void Update() {
		if (!canMove) {
			axisH = 0;
			axisV = 0;
		}
		moveDirection = new Vector3(axisH,axisV,0).normalized;

	}
	
	void FixedUpdate() {
		if(canMove)
		GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + Camera.main.transform.TransformDirection(moveDirection) * moveSpeed * Time.deltaTime );
	}
}
