﻿using UnityEngine;
using System.Collections;

public class Activation : MonoBehaviour {
	
	public PlayerController controller;
	
	void OnTriggerEnter(Collider other)
	{ if (other.tag == "Player")
		controller.enabled = true;
		
	}
}