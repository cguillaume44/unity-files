﻿using UnityEngine;
using System.Collections;
using CnControls;

public class PlayerControllerB : MonoBehaviour {
	
	public DpadAxis1 Dpad1;
	public float moveSpeed = 70;
	private Vector3 moveDirection;
	public int axisH;
	public int axisV;
	public bool canMove;
	
	void Update() {
		
		moveDirection = new Vector3(axisH,0,axisV);
	}
	
	void FixedUpdate() {
		if(canMove)
		GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.TransformDirection(moveDirection) * moveSpeed * Time.deltaTime);
	}
}
